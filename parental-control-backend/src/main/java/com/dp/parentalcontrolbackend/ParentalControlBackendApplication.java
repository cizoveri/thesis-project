package com.dp.parentalcontrolbackend;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ParentalControlBackendApplication {

	public static void main(String[] args) {
		SpringApplication.run(ParentalControlBackendApplication.class, args);
	}
}
